#!/usr/bin/env pythoh3

'''
Program to produce a triangle with numbers
'''

import sys


def line(number: int):
    """Return a string corresponding to the line for number"""
    linea=""
    for i in range(1,number+1):
        linea+=str(number)
    linea += "\n"
    return linea

def triangle(number: int):
    """Return a string corresponding to the triangle for number"""
    cadena=""
    for i in range(1, number+1):
        cadena+=line(i)
    return(cadena)

def main():
    number: int = sys.argv[1]
    if int(number)>0 and int(number)<10:
        text=triangle(int(number))
        print(text)
    else: print("ERROR")

if __name__ == '__main__':
    main()
